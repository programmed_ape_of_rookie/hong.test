package com.tee.commen.ex.sys;

import com.tee.commen.ex.ServiceException;

public class UsernameFormatException extends ServiceException {
    public UsernameFormatException() {
        super();
    }

    public UsernameFormatException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public UsernameFormatException(String message) {
        super(message);
    }

    public UsernameFormatException(Throwable cause) {
        super(cause);
    }
}
