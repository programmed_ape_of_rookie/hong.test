package com.tee.commen.ex.sys;

import com.tee.commen.ex.ServiceException;

public class UserExistException extends ServiceException {
    public UserExistException() {
        super();
    }

    public UserExistException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public UserExistException(String message) {
        super(message);
    }

    public UserExistException(Throwable cause) {
        super(cause);
    }
}
