package com.tee.commen.ex.sys;

import com.tee.commen.ex.ServiceException;

public class UnknowUserException  extends ServiceException {
    public UnknowUserException() {
        super();
    }

    public UnknowUserException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public UnknowUserException(String message) {
        super(message);
    }

    public UnknowUserException(Throwable cause) {
        super(cause);
    }
}
