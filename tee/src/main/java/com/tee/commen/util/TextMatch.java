package com.tee.commen.util;

public class TextMatch {
    //用户名格式校验首字母开头，长度3-15位
    private static final String REGX_USERNAME = "[a-zA-Z]{1}[a-zA-Z0-9_]{3,15}";
    public static boolean checkUsername(String username) {
        if (username == null) {
            return false;
        }
        return username.matches(REGX_USERNAME);
    }
}
