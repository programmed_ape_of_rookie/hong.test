package com.tee.dao.sale;

import com.tee.entity.sale.SaleCustomer;

public interface SaleCustomerMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SaleCustomer record);

    int insertSelective(SaleCustomer record);

    SaleCustomer selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SaleCustomer record);

    int updateByPrimaryKey(SaleCustomer record);
}