package com.tee.dao.sys;

import com.tee.entity.sys.SysUsers;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SysUsersMapper {
    //根据用户ID删除用户
    int deleteByPrimaryKey(Long id);
    //插入用户全部数据
    int insert(SysUsers record);
    //插入用户部分数据
    int insertSelective(SysUsers record);
    //根据主键读取用户数据
    SysUsers selectByPrimaryKey(Long id);
    //根据用户信息更新用户部分数据
    int updateByPrimaryKeySelective(SysUsers record);
    //根据用户信息更新用户数据
    int updateByPrimaryKey(SysUsers record);
    //根据用户查找用户
    SysUsers selectByUsername(String username);

    List<SysUsers> selectAllUser();

    List<SysUsers> findPageObjects(@Param("startIndex")int startIndex,
                                         @Param("pageSize") int pageSize);
    int getRowCount();
}