package com.tee.dao.base;

import com.tee.entity.base.BaseEmployee;

public interface BaseEmployeeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BaseEmployee record);

    int insertSelective(BaseEmployee record);

    BaseEmployee selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BaseEmployee record);

    int updateByPrimaryKey(BaseEmployee record);
}