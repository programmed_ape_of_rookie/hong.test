package com.tee.dao.base;

import com.tee.entity.base.BaseGoods;

public interface BaseGoodsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(BaseGoods record);

    int insertSelective(BaseGoods record);

    BaseGoods selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(BaseGoods record);

    int updateByPrimaryKeyWithBLOBs(BaseGoods record);

    int updateByPrimaryKey(BaseGoods record);
}