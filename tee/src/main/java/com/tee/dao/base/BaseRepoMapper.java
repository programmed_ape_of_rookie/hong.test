package com.tee.dao.base;

import com.tee.entity.base.BaseRepo;

public interface BaseRepoMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BaseRepo record);

    int insertSelective(BaseRepo record);

    BaseRepo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BaseRepo record);

    int updateByPrimaryKeyWithBLOBs(BaseRepo record);

    int updateByPrimaryKey(BaseRepo record);
}