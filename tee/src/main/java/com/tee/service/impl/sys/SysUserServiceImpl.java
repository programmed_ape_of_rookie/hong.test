package com.tee.service.impl.sys;

import com.tee.commen.ex.sys.UserExistException;
import com.tee.commen.vo.PageObject;
import com.tee.dao.sys.SysUsersMapper;
import com.tee.entity.sys.SysUsers;
import com.tee.service.sys.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class SysUserServiceImpl implements SysUserService {
    /**
     *
     */
    @Autowired
    private SysUsersMapper sysUsersMapper;



    /**
     * 增加用户数据
     * @param username
     * @param password
     * @return
     */
    @Override
    public SysUsers insert(String username,
                           String password,
                           Long roleId) {

       //判断用户是否存在
        SysUsers sysUsers = sysUsersMapper.selectByUsername(username);
        if (sysUsers != null){
            throw new UserExistException();
        }
        //创建用户实例
        sysUsers = new SysUsers();
        //设置用户名
        sysUsers.setUsername(username);
        //设置盐值
        String salt = UUID.randomUUID().toString();
        sysUsers.setSalt(salt);
        //密码加密
        password = DigestUtils.md5DigestAsHex((password+salt).getBytes());
        sysUsers.setPassword(password);
        //设置用户管理权限
        sysUsers.setRoleId(roleId);
        //创建人
        sysUsers.setModifiedName("admin");
        //创建时间
        sysUsers.setModifiedTime(new Date());
        int row = sysUsersMapper.insert(sysUsers);
        return sysUsers;
    }
    @Override
    public PageObject selectAllUser(int pageCurrent) {
        Integer pageSize=3;
        Integer startIndex=(pageCurrent-1)*pageSize;
        List<SysUsers> list = sysUsersMapper.findPageObjects(startIndex,pageSize);
        PageObject po=new PageObject();
        Integer pageCount = sysUsersMapper.getRowCount();
        po.setRowCount(pageCount);
        po.setRecords(list);
        po.setPageSize(pageSize);
        po.setPageCurrent(pageCurrent);
        po.setPageCount((pageCount-1)/pageSize+1);
        return po;
    }

    @Override
    public int updateUser() {
        return 0;
    }

}
