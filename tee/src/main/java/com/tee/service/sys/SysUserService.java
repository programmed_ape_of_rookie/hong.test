package com.tee.service.sys;

import com.tee.commen.vo.PageObject;
import com.tee.entity.sys.SysUsers;

import java.util.List;

public interface SysUserService {

    PageObject selectAllUser(int pageCurrent);

    int updateUser();

    SysUsers insert(String username,
                    String password,
                    Long roleId);
}
