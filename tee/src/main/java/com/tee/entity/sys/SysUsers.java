package com.tee.entity.sys;

import java.util.Date;

public class SysUsers {
    private Long id;

    private String username;

    private String password;

    private String salt;

    private Long roleId;

    private String roleName;

    private Long employeeId;

    private String modifiedName;

    private Date modifiedTime;
    //1,正常使用，0,锁定
    private Byte state = 1;

    private Byte isLock = 0;

    private Date lockTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt == null ? null : salt.trim();
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName == null ? null : roleName.trim();
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getModifiedName() {
        return modifiedName;
    }

    public void setModifiedName(String modifiedName) {
        this.modifiedName = modifiedName == null ? null : modifiedName.trim();
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public Byte getState() {
        return state;
    }

    public void setState(Byte state) {
        this.state = state;
    }

    public Byte getIsLock() {
        return isLock;
    }

    public void setIsLock(Byte isLock) {
        this.isLock = isLock;
    }

    public Date getLockTime() {
        return lockTime;
    }

    @Override
    public String toString() {
        return "SysUsers{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", salt='" + salt + '\'' +
                ", roleId=" + roleId +
                ", roleName='" + roleName + '\'' +
                ", employeeId=" + employeeId +
                ", modifiedName='" + modifiedName + '\'' +
                ", modifiedTime=" + modifiedTime +
                ", state=" + state +
                ", isLock=" + isLock +
                ", lockTime=" + lockTime +
                '}';
    }

    public void setLockTime(Date lockTime) {
        this.lockTime = lockTime;
    }
}