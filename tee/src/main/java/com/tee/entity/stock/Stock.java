package com.tee.entity.stock;

import java.util.Date;

public class Stock {
    private Long id;

    private Long goodId;

    private Long repo;

    private Integer totalCount;

    private Integer saleCount;

    private Long buyPrice;

    private Long salePrice;

    private Long avgBugPrice;

    private Long totalBuyPrice;

    private Long totalSalePrice;

    private String modifiedUser;

    private Date modifiedTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGoodId() {
        return goodId;
    }

    public void setGoodId(Long goodId) {
        this.goodId = goodId;
    }

    public Long getRepo() {
        return repo;
    }

    public void setRepo(Long repo) {
        this.repo = repo;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getSaleCount() {
        return saleCount;
    }

    public void setSaleCount(Integer saleCount) {
        this.saleCount = saleCount;
    }

    public Long getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(Long buyPrice) {
        this.buyPrice = buyPrice;
    }

    public Long getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Long salePrice) {
        this.salePrice = salePrice;
    }

    public Long getAvgBugPrice() {
        return avgBugPrice;
    }

    public void setAvgBugPrice(Long avgBugPrice) {
        this.avgBugPrice = avgBugPrice;
    }

    public Long getTotalBuyPrice() {
        return totalBuyPrice;
    }

    public void setTotalBuyPrice(Long totalBuyPrice) {
        this.totalBuyPrice = totalBuyPrice;
    }

    public Long getTotalSalePrice() {
        return totalSalePrice;
    }

    public void setTotalSalePrice(Long totalSalePrice) {
        this.totalSalePrice = totalSalePrice;
    }

    public String getModifiedUser() {
        return modifiedUser;
    }

    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser == null ? null : modifiedUser.trim();
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }
}