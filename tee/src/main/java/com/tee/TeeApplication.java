package com.tee;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.tee.dao")
public class TeeApplication {

    public static void main(String[] args) {
        SpringApplication.run(TeeApplication.class, args);
    }

}
