package com.tee.controller;

import com.tee.commen.ex.ServiceException;
import com.tee.commen.ex.sys.*;
import com.tee.commen.vo.JsonResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 异常控制基类
 */
@RestControllerAdvice
public class BaseController {
    @ExceptionHandler(ServiceException.class)
    public JsonResult ServiceException(
            Exception e){
        e.printStackTrace();//给谁看?
        JsonResult r=new JsonResult();
        if (e instanceof UnknowUserException){
            return new JsonResult(401,"用户名密码错误");
        }else if (e instanceof UsernameFormatException){
            return new JsonResult(402,"用户名格式错误");
        }else if(e instanceof  UserExistException){
            return  new JsonResult(403,"用户名已经存在");
        }
        return null;
    }
}
