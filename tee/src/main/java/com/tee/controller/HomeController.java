package com.tee.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 页面控制基类
 */
@Controller
public class HomeController {
    @RequestMapping("userlist")
    public String userList(){
        return "dist/userlist.html";
    }
    @RequestMapping("/page")
    public String page(){
        return "dist/page.html";
    }
}
