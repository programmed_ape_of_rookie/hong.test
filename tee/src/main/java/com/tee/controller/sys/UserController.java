package com.tee.controller.sys;

import com.tee.commen.ex.sys.UsernameFormatException;
import com.tee.commen.util.TextMatch;
import com.tee.commen.vo.JsonResult;
import com.tee.entity.sys.SysUsers;
import com.tee.service.sys.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 用户数据控制器
 */
@Controller
@RequestMapping("/sys")
public class UserController {
    @Autowired
    private SysUserService sysUserService;

    /**
     * 登录功能API
     * @param username 用户名
     * @param password 密码
     * @return
     */
    @PostMapping("/login")
    @ResponseBody
    public String baseLogin(String username,
                            String password){

        return  "username:"+username+"         password:"+password;
    }
    /**
     * 用户注册API
     * @param username 用户名
     * @param password 密码
     * @return
     */
    @PostMapping("/reg")
    @ResponseBody
    public JsonResult baseReg(String username,
                              String password,
                              Long roleid){
        if (!TextMatch.checkUsername(username)){
            throw new UsernameFormatException();
        }
        JsonResult<SysUsers> jsonResult = new JsonResult();
        jsonResult.setData(sysUserService.insert(username,password,roleid));
        return jsonResult;
    }

    @PostMapping("/update")
    @ResponseBody
    public JsonResult baseUpdate(){
        JsonResult jsonResult = new JsonResult();
        return jsonResult;
    }

    @GetMapping("/allUser")
    @ResponseBody
    public JsonResult<List<SysUsers>> baseAllUser(int pageCurrent,
                                                  String username){
        JsonResult jsonResult = new JsonResult();
        jsonResult.setData(sysUserService.selectAllUser(pageCurrent));
        return jsonResult;
    }
}
